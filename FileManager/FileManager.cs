﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace InputOutput
{
    public class FileManager
    {
        #region private attributs
        //TODO Declare private attributs
        #endregion private attributs

        #region constructors
        /// <summary>
        /// This constructors return a new instance of the FileManager class.
        /// </summary>
        /// <param name="path";
        /// <param name="fileName"></param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when the full path doesn't exist</exception>
        public FileManager(string path, string fileName)
        {
            //TODO Constructor
        }
        #endregion constructors

        #region accessors and mutators
        public List<string> Lines { get => null;}//TODO Accessors
        #endregion accessors and mutators

        #region public methods
        /// <summary>
        /// This method reads and extracts the file content.
        /// </summary>
        /// <param name="fileFullPath"></param>
        /// <exception cref="FileManagerEmptyFileException">Thrown when the file is empty</exception>
        //https://docs.microsoft.com/en-us/dotnet/api/system.io.streamreader?view=netframework-4.8
        public void ExtractFileContent()
        {
            //TODO ExtractFileContent
        }

        /// <summary>
        /// This method splits the original content (lines) in several files.
        /// The parameter "fileSize" defined the amount of lines per file.
        /// </summary>
        /// <param name="fileSize">Amount of line per file</param>
        public void Split(int fileSize)
        {
            //TODO Split Method
        }
        #endregion public methods

    }

    public class FileManagerException : Exception { }

    public class FileManagerEmptyFileException : FileManagerException { }

    public class FileManagerStructureException : FileManagerException { }

}
