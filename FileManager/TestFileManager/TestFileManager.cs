﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using InputOutput;

namespace TestFileManager
{
    [TestClass]
    public class TestFileManager
    {
        #region private attributs
        private FileManager fileManager;
        private static string path = Directory.GetCurrentDirectory();
        private static string fileName = "testFile.csv";
        #endregion private attributs

        [TestInitialize]
        public void Init()
        {
            if (File.Exists(path + "//" + fileName))
            {
                File.Delete(path + "//" + fileName);
            }
            StreamWriter streamWriter = new StreamWriter(path + "//" + fileName);
            streamWriter.Close();
        }

        /// <summary>
        /// This test validates the constructor's behavior.
        /// Test case : try to open an inexisting file
        /// </summary>
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        [TestMethod]
        public void Constructor_wrongFullPath_ThrowFileNotFoundException()
        {
            //given
            string wrongPath = "falkjalj";
            string wrongFileName = "wrong.csv";

            //when
            this.fileManager = new FileManager(wrongPath, wrongFileName);

            //then
            //exception thrown
        }

        /// <summary>
        /// This test validates the constructor's behavior.
        /// Test case : File is empty.
        /// </summary>
        [ExpectedException(typeof(FileManagerEmptyFileException))]
        [TestMethod]
        public void Constructor_FileEmpty_ThrowFileEmptyFoundException()
        {
            //given
            //refer to Init() method
            this.fileManager = new FileManager(path, fileName);

            //when
            this.fileManager.ExtractFileContent();

            //then
            //exception thrown
        }

        /// <summary>
        /// This test validates the Split method's behavior.
        /// Test case : One big file (400 lines) splitted in two files (200 lines each)
        /// </summary>
        [ExpectedException(typeof(FileManagerEmptyFileException))]
        [TestMethod]
        public void Split_OnlyOneBigFile_Success()
        {
            //given
            //refer to Init() 
            int amountOfLinesInOriginalFile = 400;
            int expectedAmountOfResultFiles = 2;
            int expectedLinesPerFiles = amountOfLinesInOriginalFile / expectedAmountOfResultFiles;
            StreamWriter streamWriter = new StreamWriter(path + "//" + fileName);
            for (int i = 1; i >= amountOfLinesInOriginalFile; i++)
            {
                streamWriter.WriteLine(i);
            }
            streamWriter.Close();

            this.fileManager = new FileManager(path, fileName);
            this.fileManager.ExtractFileContent();

            //when
            this.fileManager.Split(expectedLinesPerFiles);
            string[] listOfFilesResult = Directory.GetFiles(path, "Split*.csv");

            //then
            Assert.AreEqual(expectedAmountOfResultFiles, listOfFilesResult.Length);
        }

        [TestCleanup]
        public void Cleanup()
        {
            string[] listOfFilesResult = Directory.GetFiles(path, "*.csv");
            foreach (string file in listOfFilesResult)
            {
                File.Delete(file);
            }
        }
    }
}
