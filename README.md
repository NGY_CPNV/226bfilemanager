# README #

Ce projet "FileManager" est un projet permettant d'entraîner l'approche TDD (Test Driven Development).

### Objectif de ce dépôt ? ###

Il s'agit d'un dépôt créé à des fins pédagogiques.

Les compétences suivantes sont ciblées:

* Coder en partant des tests
* Ecrire des tests

### Comment débuter ? ###

* Cloner le dépôt
* L'ouvrir dans Visual Studio
* Afficher la task list ainsi :

![Task List](./DisplayTaskList.PNG)

### A qui parler en cas de question ###

L'option "issue" a été activée sur ce dépôt.